﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace Multithreading
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Stopwatch timer = new Stopwatch();
                int elementsCount = 10_000_000;
                long result = 0;

                long[] mas = GenerateMassive(elementsCount);

                timer.Start();
                result = BaseSum(mas);
                timer.Stop();
                Console.WriteLine($"Обычное вычисление суммы элементов массива: " +
                    $"затраченное время {timer.ElapsedMilliseconds} мс, " +
                    $"сумма {result}");

                timer.Restart();
                result = ParallelSum(mas);
                timer.Stop();
                Console.WriteLine($"Параллельное вычисление суммы элементов массива: " +
                    $"затраченное время {timer.ElapsedMilliseconds} мс, " +
                    $"сумма {result}");

                timer.Restart();
                result = ParallelLinqSum(mas);
                timer.Stop();
                Console.WriteLine($"Параллельное (LINQ) вычисление суммы элементов массива: " +
                    $"затраченное время {timer.ElapsedMilliseconds} мс, " +
                    $"сумма {result}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Метод генерирует массив случайных целых чисел.
        /// </summary>
        /// <param name="elementsCount">Количество элементов массива.</param>
        /// <returns>Массив случайных целых чисел.</returns>
        static long[] GenerateMassive(int elementsCount)
        {
            long[] massive = new long[elementsCount];
            Random r = new Random();

            for (int i = 0; i < elementsCount; i++)
            {
                massive[i] = r.Next(1, elementsCount);
            }

            return massive;
        }

        /// <summary>
        /// Базовый расчет суммы элементов массива.
        /// </summary>
        /// <param name="mas">Массив случайных целых чисел.</param>
        /// <returns>Сумма элементов массива.</returns>
        static long BaseSum(long[] mas)
        {
            long result = 0;
            for (int i = 0; i < mas.Length; i++)
            {
                result += mas[i];
            }

            return result;
        }

        /// <summary>
        /// Параллельный расчет суммы элементов массива случайных целых чисел.
        /// </summary>
        /// <param name="mas">Массив случайных целых чисел.</param>
        /// <returns>Сумма элементов массива.</returns>
        static long ParallelSum(long[] mas)
        {
            long result = 0;
            Parallel.For<long>(0, mas.Length, () => 0, (j, loop, subtotal) =>
            {
                subtotal += mas[j];
                return subtotal;
            },
            (x) => Interlocked.Add(ref result, x)
            
            );
            return result;
        }

        /// <summary>
        /// Параллельный расчет суммы элементов массива случайных целых чисел.
        /// </summary>
        /// <param name="mas">Массив случайных целых чисел.</param>
        /// <returns>Сумма элементов массива.</returns>
        static long ParallelLinqSum(long[] mas)
        {
            return mas.AsParallel().Sum();
        }
    }
}
